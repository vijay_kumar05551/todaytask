//
//  ViewController.m
//  todayTask
//
//  Created by Click Labs130 on 11/4/15.
//  Copyright (c) 2015 vijay kumar. All rights reserved.
//

#import "ViewController.h"
#import <GoogleMaps/GoogleMaps.h>
CLLocationManager *manager;
NSString *arrayData;
NSMutableArray *city;
NSMutableArray *destData;
GMSCameraPosition *camera;
@interface ViewController () <CLLocationManagerDelegate>{
    CLLocationCoordinate2D position;

}
@property (strong, nonatomic) IBOutlet UIView *gMapView;
@property (strong, nonatomic) IBOutlet UIButton *submitButton;



@end

@implementation ViewController

@synthesize gMapView;
@synthesize submitButton;
GMSMapView *mapView_;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    manager=[[CLLocationManager alloc]init];
    manager.delegate=self;
     manager.desiredAccuracy=kCLLocationAccuracyBest;
    [manager requestAlwaysAuthorization];
    [manager startUpdatingLocation];
   
}
- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation %@",newLocation);
    position=newLocation.coordinate;
    GMSMarker *marker=[GMSMarker markerWithPosition:position];
    marker.icon=[GMSMarker markerImageWithColor:[UIColor greenColor]];
   // marker.title=[NSString stringWithFormat:@"%@",arrayData];
    marker.map=gMapView;
    
    [self location];
    
}
-(void)location
{
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    
    CLLocation *newLocation = [[CLLocation alloc]initWithLatitude:position.latitude
                                                        longitude:position.longitude];
    
    [geocoder reverseGeocodeLocation:newLocation
                   completionHandler:^(NSArray *placemarks, NSError *error) {
                       
                       if (error) {
                           NSLog(@"Geocode failed with error: %@", error);
                           return;
                       }
                       
                       if (placemarks && placemarks.count > 0)
                       {
                           CLPlacemark *placemark = placemarks[0];
                           
                           NSDictionary *addressDictionary =
                           placemark.addressDictionary;
                           
                           NSLog(@"%@ ", addressDictionary) ;
                           NSString *cityString=addressDictionary[@"City"];
                           NSLog(@"%@",cityString);
                           NSString *countryString=addressDictionary[@"Country"];
                           NSLog(@"%@",countryString);
                           NSString *formattedAddressString=addressDictionary[@"FormattedAddressLines"];
                           NSLog(@"%@",formattedAddressString);
                           NSString *stateString=addressDictionary[@"State"];
                           NSLog(@"%@",stateString);
                           
                       }
                       
                   }];}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"nxtScreen"]) {
   destinationViewController *dest= segue.destinationViewController;
    dest.destData=city;
        
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
